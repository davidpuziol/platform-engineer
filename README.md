# platform-engineer

Esse repositório visa no estudo sobre o tema platform engineer

## Conceitos iniciais

### O que é SRE?

Site Realiability Engineering, ou seja, engenharia de confiabilidade.
Esse termo refere-se a um profisional com aspectos de engenharia no time de operações para garantir a confiabilidade e disponibilidade do produto (software).

É esperando que um profissional de SRE meça e garanta a qualidade do serviço (QoS Quality of Service) oferecido, seja de um produto interno ou externo da empresa.

Não é somente sobre infra estrutura, mas sim sobre a organização.

Na pasta [books](./books/) temos 3 livros de grande importância para este tema.

### Métricas

Quando falamos de um produto de software presumimos que devemos aceitar riscos e mudar o nosse `Se isso acontecer` para `Quando isso acontecer`, pois um dia irá acontecer e devemos nos preparar.

As principais métricas de confiabilidade, que podem ser definidos com ACORDOS são:

- SLA: Service Level Agreement é o que foi previsto em contrato. Não é nada mais do que a obrigação e nunca pode ser quebrado, pois pode gerar danos legais para a empresa.
- SLO: Service Level Objective é um objetivo criado para melhorar a métrica do SLA. Precisa ser mais rigoroso do que o SLA e também não pode ser 100%. Se o SLA promete 99% de disponiblidade como por exemplo um serviço de cloud, o SLO tem que ser 99.9%. Uma vez que a métrica do SLO chega em 100% das vezes ele vira o SLA até que vire o contrato.
- SLI: Service Level Indicator são as métricas vivas dos indicativos do SLA e SLO.
- Error Budget é o quanto falta para o SLO e SLA serem quebrados, ou seja é margem de erro.

Os valores das métricas devem ser um indicador que todos entendam, não só do time técnico, ou seja, um número é o suficiente. Se alguém demorar mais de 30 segundos pra entender a métrica então não é uma boa métrica.

A idéia que que vire um número que a empresa irá abraçar para alcançar.

Uma boa idéia é tentar mostrar essa métrica para alguem de fora do processo, da empresa e ver se ela entendeu.

As métricas de observabilidade de um lindo dashboard do grafana, medindo tracing, processamento, etc, servem para ajudar a chegar no SLO ou corrigir algum problema para mantê-lo.

Uma boa métrica é para começar são as métricas RED que são simples e funcionam para qualquer protocolo http

- Rate : Quantidade de requests por um range de tempo
- Error : Contagem de erros no range de tempo
- Duration : Duração do tempo de resposta.
  - Nesse caso é bom sempre trabalhar com p99 para receber uma resposta, ou seja 99% estão dentro do tempo máximo de resposta.





crosssplane backstage

What is Backstage?
https://www.crossplane.io/
https://backstage.io/docs/overview/what-is-backstage
